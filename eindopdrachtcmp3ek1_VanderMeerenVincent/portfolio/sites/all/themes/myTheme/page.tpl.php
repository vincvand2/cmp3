<div class="wrapper" id="header-wrapper">
    <div class="container">
        <div class="row">
            <header id="header">
                <section id="header-logo" class="col col-xxs-12">
                    <div id="logo">
                    </div>
                    <h2><?php print $site_slogan ?></h2>
                </section>
                <nav id="nav">
                    <ul>
                        <?php if ($main_menu): ?>
                            <div id="main-menu" class="navigation">
                                <?php print theme('links__system_main_menu', array(
                                    'links' => $main_menu,
                                    'attributes' => array(
                                        'id' => 'main-menu-links',
                                        'class' => array('links', 'clearfix'),
                                    ),
                                    'heading' => array(
                                        'text' => t('Main menu'),
                                        'level' => 'h2',
                                        'class' => array('element-invisible'),
                                    ),
                                )); ?>
                            </div> <!-- /#main-menu -->
                        <?php endif; ?>
                    </ul>
                </nav>
            </header>
        </div>
    </div>
</div>

<div class="wrapper" id="home">
    <div class="container">
        <div class="row">
            <section id="tweets-all" class="tweets col col-xxs-12">
               <?php if($page['content']): ?>
                    <?php print render($page['content']); ?>
                <?php endif; ?>
            </section>
        </div>
    </div>
</div>


<div class="footer wrapper">
    <div class="container">
        <div class="row">
            <!--<p>&copy; Vincent Van der Meeren - 2015</p>
            <a href="http://localhost:8080/eindwerkCMP3/rss.xml">RSS</a>
            <a href="http://localhost:8080/eindwerkCMP3/node/25">Diclaimer</a>
            <a href="http://localhost:8080/eindwerkCMP3/node/24">Privacy Policy</a>-->
            <?php if($page['footer']): ?>
                <?php print render($page['footer']); ?>
            <?php endif; ?>
        </div>
    </div>
</div>